﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Kata.TheTeaParty;

namespace TestLibrary
{
    [TestFixture]
    public class TheTeaPartyTests
    {
        private TeaPartyKata _kata;

        [TestFixtureSetUp]
        public void SetupTest()
        {
            _kata = new TeaPartyKata();
        }

        [Test]
        public void IsThisAMan()
        {
            //Arrange
            bool IsWoman = false;
            bool IsKnighted = false;

            string expectedTitle = "Mr";

            //Act
            var returnedTitle = _kata.DecipherGreeting(IsWoman, IsKnighted);
            
            //Assert
            Assert.AreEqual(expectedTitle, returnedTitle);

        }

        [Test]
        public void IsThisAWomen()
        {
            //Arrange
            bool IsWoman = true;
            bool IsKnighted = false;

            string expectedTitle = "Mrs";

            //Act
            var returnedTitle = _kata.DecipherGreeting(IsWoman, IsKnighted);
            
            //Assert
            Assert.AreEqual(expectedTitle, returnedTitle);

        }

        [Test]
        public void AreTheyAKnightOfTheRealm()
        {
            //Arrange
            bool IsWoman = false;
            bool IsKnighted = true;

            string expectedTitle = "Sir";

            //Act
            var returnedTitle = _kata.DecipherGreeting(IsWoman, IsKnighted);

            //Assert
            Assert.AreEqual(expectedTitle, returnedTitle);

        }

        [Test]
        public void ShouldGreetKnight()
        {
            //Arrange
            string lastName = "lastName";
            bool isWomen = false;
            bool isKnightOfRealm = true;

            string expectedGreeting = "Hello Sir lastName";

            //Act
            string GivenGreeting = _kata.Welcome(lastName, isWomen, isKnightOfRealm);

            //Assert
            Assert.AreEqual(expectedGreeting, GivenGreeting);
        }

        [Test]
        public void ShouldGreetWomen()
        {
            //Arrange
            string lastName = "lastName";
            bool isWomen = true;
            bool isKnightOfRealm = false;

            string expectedGreeting = "Hello Mrs lastName";

            //Act
            string GivenGreeting = _kata.Welcome(lastName, isWomen, isKnightOfRealm);

            //Assert
            Assert.AreEqual(expectedGreeting, GivenGreeting);
        }

        [Test]
        public void ShouldGreetMan()
        {
            //Arrange
            string lastName = "lastName";
            bool isWomen = false;
            bool isKnightOfRealm = false;

            string expectedGreeting = "Hello Mr lastName";

            //Act
            string GivenGreeting = _kata.Welcome(lastName, isWomen, isKnightOfRealm);

            //Assert
            Assert.AreEqual(expectedGreeting, GivenGreeting);
        }
    }
}
