﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.TheTeaParty
{
    public class Program
    {
        static void Main()
        {
            TeaPartyKata kata = new TeaPartyKata();
            
            Console.WriteLine(kata.Welcome("lastname", false, true));

            //keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }

    public class TeaPartyKata : ITeaPartyKata
    {
        public const string HELLO = "Hello";
        public const string KEY_VAL_PAIR = "{0} {1} {2}";

        public string Welcome(string lastName, bool isWoman, bool isSir)
        {

            string greeting = string.Format(KEY_VAL_PAIR, HELLO,DecipherGreeting(isWoman, isSir),lastName);
            return greeting;
        }

        public string DecipherGreeting(bool isWoman, bool isKnightOfThemRealm)
        {
            if (isWoman)
                return "Mrs";

            if (isKnightOfThemRealm)
                return "Sir";
                           
           return "Mr";
        }
    }

    public interface ITeaPartyKata
    {
        /// <summary>
        /// Welcome a guest
        /// </summary>
        /// <param name="lastName">the last name of the guest</param>
        /// <param name="isWoman"><c>true</c> if the guest is female</param>
        /// <param name="isSir"><c>true</c> if the guest was knighted by the queen</param>
        /// <returns>issues welcome text to the guest</returns>
        string Welcome(string lastName, bool isWoman, bool isSir);
    }


}
